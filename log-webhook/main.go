package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strings"

	"github.com/sirupsen/logrus"
)

const(
	URL=""
)


func main(){
	msg:="i am test msg"
	SendMessage(msg)
}

type Message struct {
	MsgType string `json:"msgtype"`
	Text struct {
		Content string `json:"content"`
	} `json:"text"`
}


func SendMessage(msg string) {
	var m Message
	m.MsgType = "text"
	m.Text.Content = msg
	jsons, err := json.Marshal(m)
	if err != nil {
		logrus.Error("SendMessage Marshal failed", err)
		return
	}
	resp := string(jsons)
	client := &http.Client{}
	req, err := http.NewRequest("POST", URL, strings.NewReader(resp))
	if err != nil {
		logrus.Error("SendMessage http NewRequest failed", err)
		return
	}
	req.Header.Set("Content-Type", "application/json")
	r, err := client.Do(req)
	if err != nil {
		logrus.Error("SendMessage client Do failed", err)
		return
	}
	defer r.Body.Close()
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		logrus.Error("SendMessage ReadAll Body failed", err)
		return
	}
	logrus.Info("SendMessage success", string(body))
}