package main

import (
	"fmt"

	"github.com/antlinker/go-dirtyfilter"
	"github.com/antlinker/go-dirtyfilter/store"
)

var (
	filterText = `我是需要过滤的内容，内容为：**文@@件，需要过滤。。。`
)

func main() {
	mongoStore, err := store.NewMongoStore(store.MongoConfig{
		URL:"mongodb://root:123456@xx.xx.xx.xx:27017",
		DB: "users",
		Collection: "c_word-filter",
	})
	if err != nil {
		panic(err)
	}
	words, err := mongoStore.ReadAll()
	if err != nil {
		panic(err)
	}

	memStore, err := store.NewMemoryStore(store.MemoryConfig{
		DataSource: words,
	})
	if err != nil {
		panic(err)
	}

	filterManage := filter.NewDirtyManager(memStore)
	result, err := filterManage.Filter().Filter(filterText, '*', '@')
	if err != nil {
		panic(err)
	}
	fmt.Println(result)
}